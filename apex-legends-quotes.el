;;; package --- Summary
;;; apex-legends-quotes.el ---
;;
;; apex-legends-quotes.el is part of apex-legends-quotes
;; Description: Have a random [[https://www.ea.com/games/apex-legends/play-now-for-free][Apex Legends]]
;; quote as frame title
;; Author: Justine Kizhakkinedath
;; Maintainer: Justine Kizhakkinedath <justine@kizhak.com>
;; Copyright (c) 2020, Justine Kizhakkinedath
;; All rights reserved
;; Created: Sat  4 Jan 2020 13:26:56 IST
;; Version: v0.1
;; Package-Requires: ()
;; Last-Updated: Sun  5 Jan 2020 18:22:02 IST
;;           By:
;;     Update #: 56
;; URL: Not available
;; Doc URL:
;; Keywords:
;; Compatibility: Emacs 26.3
;;
;; -----------------------------------------------------------------------------
;;
;;; Commentary:
;;
;;
;;
;; -----------------------------------------------------------------------------
;;
;;; Change Log:
;;
;;
;; -----------------------------------------------------------------------------
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Code:
(defgroup apex-legends nil
  "Use a random quote from Apex Legends character."
  :group 'appearance)

;; -----------------------------------------------
;; functions

(defun random-from-vector (items)
  "Return random element from ITEMS."
  (let* ((size (length items))
         (index (random size)))
    (aref items index)))

;; Mirage-----------------------------------------

(defgroup apex-legends--mirage nil
  "Customization group for mirage."
  :group 'apex-legends)

(defcustom mirage--legend-selection '(
                                      "Alright, either you're with me or you're against me, or you're with me and against me, cause I'll be honest, that happens sometimes."
                                      "It's a good day to take out some fools and be back in time for dinner! I got porkchops tonight. Love porkchops.."
                                      "Listen, I've been training all day, so, it's about time that, y'know, we did some fighting."
                                      "Now ya see me, now ya don't. Now ya see me, boom, you're dead."
                                      "Three words: Win, and win again. Yeah..."
                                      "Today will be one of those experiences where you'll sit back and say \"Oh yeah, I remember that\"."
                                      "I did my hair for this! Nothin' wrong with lookin' good in the ring."
                                      "Alright, so I did my hair for this. There's nothin' wrong with lookin' good in the ring. And I look good."
                                      "I make this look good. Literally, this hair, this face.. I mean, c'mon, you see it."
                                      "I could win this thing with one eye closed. Or both eyes closed! Hell, I don't even have to show up!"
                                      "So I like to have a little fun. So what?"
                                      "Y'know, I don't take myself too seriously. I don't take myself anywhere, really, I... man, I need to get out more."
                                      "Everything's gonna be great! Least, that's what I keep telling myself.."
                                      "Yeah, I'm ready for this. I am SO ready for this, I am extremely ready for this. ..We ready?"
                                      "Alright, I'm gonna be honest, I am afraid, I'm really afraid. I'm just kidding, nothing scares me."
                                      "I just hope I don't have to fight with that smiling robot. I'm just kiddin', he-he's fine. Everyone's fine."
                                      "Alright, let's do this. It'll be fun! If not, well, y'know, we all make bad choices, you live, you learn..."
                                      "Not gonna lie, I'm pretty good at this. Okay, I-I do lie. But not this time!"
                                      "The ring isn't really complex but it can be a little complicated. Wait, what'd I just say?"
                                      "This match is gonna be extra-vagah...extr..ext, extravagant! That's the word."
                                      "Y'know, this match is gonna be extrah, extragavent, uh, extra... e-extravagant! That's it."
                                      "I got this thing in the box! Or is it.. is it 'the bag'? ..Y'know, whatever the case, we're winnin'."
                                      )
  "List of all Mirage quotes on legend selection."
  :group 'apex-legends--mirage
  :type '(repeat string))

(defcustom mirage--legend-intro '(
                                  "Anyone wanna take me out? Come find me."
                                  "Anyone wanna take me out? Come find me. I'll buy the drinks."
                                  "I didn't get here alone. I defeated a lot of people to get here."
                                  "Y'know, I didn't get here alone. Man, I defeated a lot of people to get here."
                                  "You're gonna see a lot of me."
                                  "I-I'm speechless! Actually, I wrote a speech- y'know what, forget it, let's just fight."
                                  "This is good, this is good, let's fight!"
                                  )
  "List of all Mirage quotes on legend intro."
  :group 'apex-legends--mirage
  :type '(repeat string))

(defcustom mirage--legend-kill '(
                                 "Ah, you should've known fighting me is... is presposter—prespr—ah... pres... is dumb."
                                 "Don't blame yourself, I deserve the credit!"
                                 "Good fight. Good fight."
                                 "Hey you jumped, you ran, you shot a thing maybe, and then you met me, and... well here we are."
                                 "Hey you jumped, you ran, you shot a thing maybe, but, then you met me and well, here we are."
                                 "I gotcha."
                                 "I gotta admit... I MAY have tricked you. Phew, glad I got that off my chest."
                                 "I'm good! But, there's two of me. So I'm good twice! Check out that math."
                                 "I'm good, I'm good. What can I say? Did I say I'm good? Well I am."
                                 "I'm good, I'm good. What can I say? Did I say that I'm good 'cause... well, I am."
                                 "I'm good, but there's two of me. So I'm good TWICE! Boom, check out that math."
                                 "Most of the time one is better than the other, and both of me are better than you."
                                 "Most of the time, one is better than the other... And both of me are better than you."
                                 "Y'know I gotta admit... I may have tricked you. Glad I got that off my chest."
                                 "Yeah. I gotcha."
                                 "You should've known fighting me is prepeh—pehprehsturus... is dumb."
                                 "Ah, boy, this... was rough for you. Ah, next time maybe."
                                 "Alright, this is how it goes. You win, you lose, you die, y--... I guess that's it, really."
                                 "Free tip, just try a little bit harder. You almost had it!"
                                 "Free tip: Just try a little bit harder. Y'know you almost had it."
                                 "Hey, at least you didn't lose to the ring. I know a guy, I'll... tell you the story sometime."
                                 "Hey, at least you didn't lose to the ring. Right?"
                                 "Hey, sometimes you lose... Sometimes you're me."
                                 "Hey, thank you for this. I feel much better about myself now."
                                 "Hey, we had fun. Right?"
                                 "Hey, you did good! I think you did good, I mean... Well maybe not THAT good, after all... I mean... Look at you. Heh. Okay, bye!"
                                 "Hey, you did good. I think you did good, I mean, well—maybe not that good after all... considering... okay, goodbye."
                                 "I'll tell you this much, you DID make me break a sweat."
                                 "I'll tell you this much, you did make me break a sweat. Little bit."
                                 "I'm proud to have fought with you! I'm serious. I-- I mean it. No one ever believes me!"
                                 "I'm proud to have fought with you. I mean, seriously! I-- I mean it. (aside) Why doesn't anyone believe me...?"
                                 "I'm so sorry to make you wait, I--I mean to beat you... much faster!"
                                 "I'm so sorry to make you wait. I meant to beat you much faster."
                                 "If you ask me, you weren't... half bad. That means... you were half good!"
                                 "If you ever want tips, just give me a call."
                                 "It's not all that BAD, the suns out, it's a nice day... just trying to be hopeful."
                                 "It's not all that bad! I mean the suns out, it's a nice day, y'know... just trying to be hopeful."
                                 "Listen, quitting is for losers and since... you already lost, well there's... really no point in giving up now."
                                 "Listen, quitting is for losers! And since you already lost I mean there's... no point in giving up now."
                                 "Listen, you chose to fight me. Or... maybe was it the other way around? Ah, whatever, I won."
                                 "Listen, you chose to fight me. Or... maybe was it the other way around? Oh well! I won."
                                 "No one had your back, huh? I hate when that happens."
                                 "Not bad. If I didn't have to kill ya, I'd hire ya."
                                 "Of all the fights, this one was parti—a predi—predibum... it was a really good fight, this one, was."
                                 "Of all the fights, this was a pret—predesc—a parduh... pr-predicular—it was a really good one, is what I'm trying to say."
                                 "Oh sorry, did I confuse you?"
                                 "Okay, I'll be nice. You weren't... too bad. There. I said it. You're welcome."
                                 "Ooh boy, this was rough for you! Ah, next time maybe."
                                 "Quite the predemis--! Predah—Predepicatio—Predi—Predicamina? It's a bad situation."
                                 "Sometimes you lose, sometimes you're me."
                                 "Sorry, did I confuse you?"
                                 "Thank you for this, I feel MUCH better about myself now."
                                 "This is how it goes: You win, you lose, you die, y--... you know, I guess that's it."
                                 "This was just a LOVELY time, thank you for this."
                                 "This was... just a lovely time, thank you for this."
                                 "Trust me, losing ain't that bad, you learn something new every time! At least... that's what I've been told."
                                 "Trust me, losing ain't that bad. I mean you learn something new every time! 'Least... that's what I've been told. I wouldn't know what that's like."
                                 "Until we meet again, buddy."
                                 "Wasn't the thrill of victory in itself, really? Yeah, probably not."
                                 "Wasn't the thrill of victory in itself? Eh, probably not, you're dead."
                                 "We had fun! ...Right? I mean I had fun."
                                 "Well this is ONE way to make memories!"
                                 "Well. This is... one way to make memories."
                                 "Wow, that's quite the predemis—Pruh—Preh... Predicam—uh... It's a bad situation for you."
                                 "YOU, got bamboozled!"
                                 "Yeah, you lost this one, but it has nothing to do with your skills! 'Cause—well—I mean you don't have any, clearly."
                                 "You better not come back to haunt me! I hate when that happens."
                                 "You can't take this personally, it's just business. Personal business."
                                 "You got bamboozled!"
                                 "You gotta take the good with the bad! I'm good, and—you're bad."
                                 "You know, if you ask me, you weren't half bad. Hey! That means you were half good!"
                                 "You lost this one. But it has nothing to do with your skills! Well-- 'cause you don't have any, I mean clearly."
                                 )
  "List of all Mirage quotes on legend kills."
  :group 'apex-legends--mirage
  :type '(repeat string))

(defvar mirage--legend-voice-lines)

(setq mirage--legend-voice-lines (vconcat
                                  mirage--legend-intro
                                  mirage--legend-selection
                                  mirage--legend-kill
                                  ))

(defcustom mirage--byline  " - Elliott 'Mirage' Witt"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--mirage
  :type 'string)

(defun random-mirage-quotes ()
  "Return random Mirage quote."
  (concat (random-from-vector mirage--legend-voice-lines) mirage--byline))

;; Wraith-----------------------------------------

(defgroup apex-legends--wraith nil
  "Customization group for wraith."
  :group 'apex-legends)

(defcustom wraith--legend-intro '(
                                  "You think it's cold? Spend time in The Void. You'll know what's cold."
                                  "Follow my path to win."
                                  "I don't fear anything."
                                  "Pain... death... nothing phases me."
                                  "Try not to blink, you may miss me."
                                  "Just a step in the right direction."
                                  "Another day, another road."
                                  "Every decision counts."
                                  "Good choice... for me."
                                  "I saw this coming."
                                  "I'll take you down more than once."
                                  "I'm here today... let's do this."
                                  "I'm quick, and I'll finish this."
                                  "I've been here before."
                                  "It's my turn this time."
                                  "I know more than you'll never know."
                                  "You don't get here by sitting around doing nothing."
                                  "Something wrong with your head if you fight me."
                                  "Trust just your eyes and you'll lose."
                                  "Wanna win? Do what I do."
                                  )
  "List of all Wraith quotes on legend intro."
  :group 'apex-legends--wraith
  :type '(repeat string))

(defcustom wraith--legend-kill '(
                                 "Death... like winter... is unavoidable. "
                                 "Let's just say a little birdie told me all about you."
                                 "Can't catch what you can't see."
                                 "Caught you."
                                 "Don't blame yourself."
                                 "Every decision counts."
                                 "I caught the win this time."
                                 "I knew this end would come, just didn't know when."
                                 "I knew your every move."
                                 "I saw this coming a mile away."
                                 "I'd say try better."
                                 "I've seen this all before."
                                 "I know if we fought together, things would be different."
                                 "In another life, this may have been different."
                                 "Maybe in another time or space, you'd kill me... not this one."
                                 "It's deja vu with you fighters."
                                 "Knowledge is power... and pain."
                                 "Listen to everything... and you can win."
                                 "Looking for me? Here I am."
                                 "A match like this is much more than shooting a gun."
                                 "There were multiple ways this could end."
                                 "Not today."
                                 "Now you see me."
                                 "I guess I was just one step ahead of you."
                                 "Each win gets me one step closer to what I want."
                                 "Shoot this."
                                 "Sometimes you lose, sometimes you don't."
                                 "Step quieter next time."
                                 "Surprise."
                                 "This was the outcome this time... it's just the way it is."
                                 "The road you chose has ended here."
                                 "Don't worry, there was no way out of this."
                                 "This didn't have to happen... but it did."
                                 "This is where it ends."
                                 "Too slow."
                                 "We all have our reasons... don't take this personally."
                                 "This is what happens when you aren't focused."
                                 "You challenged the wrong person."
                                 "Be honest, you knew this would end here."
                                 "You lost this time."
                                 "Your choice is pointless... none of this matters."
                                 "Don't worry, I would've gotten you any way... zigged or zagged."
                                 )
  "List of all Wraith quotes on legend kills."
  :group 'apex-legends--wraith
  :type '(repeat string))

(defcustom wraith--legend-voices-from-void '(
                                             "I think someone's aiming at me. Not sure."
                                             "I'm pretty sure we're open targets."
                                             "Not sure, I think someone's targeting me."
                                             "Others died there!"
                                             "Pretty sure someone's got a shot on me."
                                             "Sniper, move!"
                                             "They may have seen you"
                                             "They're aiming right at you!"
                                             "They've laid traps!"
                                             "Trap nearby."
                                             "Traps!"
                                             "You're in a hot zone."
                                             )
  "List of all Wraith voices from void."
  :group 'apex-legends--wraith
  :type '(repeat string))

(defcustom wraith--legend-celebrations '(
                                         "Yes. Made it through."
                                         "Feels good when the plan actually works, nice job."
                                         "And that's how you get the job done."
                                         )
  "List of all Wraith celebrations."
  :group 'apex-legends--wraith
  :type '(repeat string))

(defvar wraith--legend-voice-lines)

(setq wraith--legend-voice-lines (vconcat
                                  wraith--legend-intro
                                  wraith--legend-kill
                                  wraith--legend-voices-from-void
                                  wraith--legend-celebrations
                                  ))

(defcustom wraith--byline " - Renee 'Wraith' Blasey"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--wraith
  :type 'string)

(defun random-wraith-quotes ()
  "Return random Wraith quote."
  (concat (random-from-vector wraith--legend-voice-lines) wraith--byline))

;; Lifeline-----------------------------------------

(defgroup apex-legends--lifeline nil
  "Customization group for lifeline."
  :group 'apex-legends)

(defcustom lifeline--voice-lines '(
                                   "A win don’t come easy, for you."
                                   "Big tings are comin’ for us."
                                   "Breathe easy, watch your side, and never quit."
                                   "Champion challenger means little if you die."
                                   "Cmon, we’ve got work to do."
                                   "Come get ya healing drone."
                                   "Come on now, ramp with me."
                                   "Don’t worry, it’s easy. Bleed, patch, and keep moving."
                                   "Get ready, today is not gonna be easy."
                                   "Gwan, try to stop me."
                                   "Ha! You’re looking at a fighter! What am I lookin’ at?"
                                   "I ain’t afraid of you, ya hear me?"
                                   "It takes a lot to take me out."
                                   "It’s only over when it’s over."
                                   "It’s time to impress."
                                   "I’ve got your back, member me tell you."
                                   "I’ve seen it all, today no different."
                                   "Last one down and a boom boom."
                                   "Let’s get the job done."
                                   "Memba dis face, it’s comin’ for ya."
                                   "Now roll with me, I’m ready for a match."
                                   "Pass me dat sugar"
                                   "Pay attention, might learn a ting or two."
                                   "Small up yaself, Ajay Che comin’ tru."
                                   "Take this loss as pride."
                                   "Take this with motivation, be betta."
                                   "This is gonna be a good fight."
                                   "This is ma win, member dat."
                                   "This is not ma first time."
                                   "This place is someting else, let’s go."
                                   "Wanna be here, take me out."
                                   "Want advice, stay outta’ ma way."
                                   "We’re in this together, remember dat."
                                   "We’ve got a job to do, so shift ya carcass."
                                   "You ain’t that bright if you wanna take me on."
                                   "You and me, let’s take this win."
                                   )
  "List of all Lifeline quotes on legend intro."
  :group 'apex-legends--lifeline
  :type '(repeat string))

(defvar lifeline--legend-voice-lines)

(setq lifeline--legend-voice-lines (vconcat
                                    lifeline--voice-lines
                                    ))

(defcustom lifeline--byline " -  Ajay 'Lifeline' Che"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--lifeline
  :type 'string)

(defun random-lifeline-quotes ()
  "Return random Lifeline quote."
  (concat (random-from-vector lifeline--legend-voice-lines) lifeline--byline))

;; Bangalore-----------------------------------------

(defgroup apex-legends--bangalore nil
  "Customization group for bangalore."
  :group 'apex-legends)

(defcustom bangalore--legend-selection '(
                                         "Real men use ironsights? Heh, I use heavy artillery."
                                         "Clips are what civvies use in their hair, this is called a magazine."
                                         "Ready up, it’s go time"
                                         "We got this round squared away"
                                         "Rook up its time to move out"
                                         "Get ready to stitch 'em up"
                                         "Nothing like cold steel on a hot day."
                                         "Spent gunpowder.. smells like victory."
                                         "No Cluster Foxtrots in my unit, let's run this."
                                         "Time to see if all those drills have paid off."
                                         "I've put the time in, I've earned my stripes."
                                         "Safety's off, weapon's free."
                                         "Lock and load, rinse, repeat. It’s that simple."
                                         "If you can't keep up you're straight out of luck."
                                         "Time to put some lead downrange."
                                         "Pop the smoke make 'em broke."
                                         "Tap, rack and clear. I'm ready."
                                         "We'll win this thing together. Semper Fi or die."
                                         "Amateur hour is over, time for the big leagues."
                                         "I came here to party, let's rock."
                                         )
  "List of all Bangalore quotes on legend intro."
  :group 'apex-legends--bangalore
  :type '(repeat string))

(defcustom bangalore--additional-lines '(
                                         "Don't mention it."
                                         "Nice, champion's out. Meat wagon is having one hell of a day."
                                         )
  "List of all Bangalore quotes on legend intro."
  :group 'apex-legends--bangalore
  :type '(repeat string))

(defvar bangalore--legend-voice-lines)

(setq bangalore--legend-voice-lines (vconcat
                                     bangalore--legend-selection
                                     bangalore--additional-lines
                                     ))

(defcustom bangalore--byline " -  Anita 'Bangalore' Williams"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--bangalore
  :type 'string)

(defun random-bangalore-quotes ()
  "Return random Bangalore quote."
  (concat (random-from-vector bangalore--legend-voice-lines) bangalore--byline))

;; Octane-----------------------------------------

(defgroup apex-legends--octane nil
  "Customization group for octane."
  :group 'apex-legends)

(defcustom octane--legend-selection '(
                                      "Reckless and full of wrecks. Let's go!"
                                      "Today's a good day to cheat death."
                                      "What are you waiting for? Let's do this!"
                                      "It's showtime!"
                                      "Scooting and looting amigo"
                                      "Let's go for a ride."
                                      "Race you to the LZ."
                                      "Stimmed up and ready to burn."
                                      "Can't stop won't stop."
                                      "Wanna fly, compadre? Let's fly."
                                      "Pick me pick me pick me I'm so bored."
                                      "I got the need for speed."
                                      "Harder, faster, stronger."
                                      "Break a leg, mine are made of steel."
                                      "These legs weren't made for walking."
                                      "I haven't found my limit."
                                      "Ready, steady, go!"
                                      "Hey, let's go already!"
                                      "I don't push the envelope, I shred it."
                                      "All aboard the Octrain."
                                      )
  "List of all Octane quotes on legend intro."
  :group 'apex-legends--octane
  :type '(repeat string))

(defvar octane--legend-voice-lines)

(setq octane--legend-voice-lines (vconcat
                                  octane--legend-selection
                                  ))

(defcustom octane--byline " -  Octavio 'Octane' Silva"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--octane
  :type 'string)

(defun random-octane-quotes ()
  "Return random Octane quote."
  (concat (random-from-vector octane--legend-voice-lines) octane--byline))

;; Wattson-----------------------------------------

(defgroup apex-legends--wattson nil
  "Customization group for wattson."
  :group 'apex-legends)

(defcustom wattson--legend-selection '(
                                       "All it takes is a single spark."
                                       "Control the charge, control the arena."
                                       "Even gunpowder needs a spark."
                                       "Fence them in, fence them out."
                                       "Fuses set and capacitors charged."
                                       "High power weapons, low power? How about, no power?"
                                       "I keep my squad mates insulated."
                                       "I know these territories like the back of my hand."
                                       "I'm ec-static to fight!"
                                       "Keep calm and don't blow a fuse."
                                       "Let's break some circuits."
                                       "Let's calm the storm."
                                       "Let's get to work."
                                       "My coil produces 1.2 million volts."
                                       "Time to cut the power."
                                       "They call me the eye of the storm."
                                       "They go where I tell them to go."
                                       "This is more than a battlefield to me."
                                       )
  "List of all Wattson quotes on legend selection."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defcustom wattson--legend-intro '(
                                   "Be careful, I don't wear all this gear for nothing."
                                   "I never say die, I fight until it hertz."
                                   "I'm current-ly at the top of my game! Get it?"
                                   "My adversaries are in for a shock."
                                   "Papa would be proud of me."
                                   "Don't feel bad. I'm pretty sure le Père Noël was leaving you coal anyway."
                                   "Didn't expect to see me up here, did you? They never do."
                                   "Electricity I get, people? I don't... That's how I got here."
                                   "Once a battle gets too noisy, I bring the silence."
                                   "It's difficult to win when I conduct the power."
                                   "I did it, Papa. I hope wherever you are, you're cheering for me."
                                   "I may not look like a fighter, but strength isn't everything."
                                   "My electric fences will herd you, like cattle."
                                   "My light shines bright."
                                   "Power is everything. You only think you have it."
                                   "Studying past combatants has served me well."
                                   "The best offense is a strong defense."
                                   "This is the only life I know."
                                   "To you, it's a gauntlet. To me, it's home, and I'll defend it to the end."
                                   "Wattage up! I'm recharged and ready to go!"
                                   "You may be stronger, but I'm smarter."
                                   "You're playing in my backyard now."
                                   )
  "List of all Wattson quotes on legend intro."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defcustom wattson--legend-kills '(
                                   "Sometimes, careful planning wins out over might."
                                   "Consider your circuit broken. Get it?"
                                   "If there's one thing I know how to do, it's to cut the power."
                                   "We're just on different sides of the fence. C’est la vie."
                                   "Don't blame yourself. I knew where you would be."
                                   "Don't blow a fuse. Get it?"
                                   "Don't confuse my kindness for weakness."
                                   "Electrons are the same, but repel each other, you and I are no different."
                                   "Go towards the light."
                                   "I didn't see you coming... but my fence did."
                                   "You fought well, but I grew up in these territories."
                                   "I guess I found your off switch."
                                   "I've been watching combatants my whole life. I learned a few things."
                                   "Some live for the chaos of battle, I prefer restoring order."
                                   "I won Papa!"
                                   "I'm the one with the stronger current."
                                   "I'm tougher than I look."
                                   "It's either you or me. Those are the rules."
                                   "Death is just the halt of electrical impulses to the brain."
                                   "Lights out."
                                   "Look on the bright side, you got beat by a legend."
                                   "Many legends fall. Time for you to join them."
                                   "My suit is insulated, you're not."
                                   "Think of this as a permanent blackout."
                                   "Size and strength aren't everything."
                                   "Somebody has to lose. That's just how this works."
                                   "I'm sorry. Sometimes the power goes out, and never comes back on."
                                   "That's the last time you charge anybody."
                                   "This is the end of your electrical current."
                                   "The legends taught me everything I know. You? Not so much."
                                   "Shh, it's okay. The noise will stop now."
                                   "There are some nodes a current cannot travel through. Sorry."
                                   "This is how it has to be. I'm sorry."
                                   "This never gets any easier, but it's how the games works."
                                   "This went exactly how I planned it to."
                                   "You burned bright, but you burned out."
                                   "You can outrun me, but you can not outrun the current."
                                   "This is what happens when you cross the wrong fence."
                                   "You didn't expect this from me-- which is why you lost."
                                   "You fought hard, but I've been studying this place my whole life."
                                   "You get too close to a live wire, and you pay the price."
                                   "You underestimated me."
                                   )
  "List of all Wattson quotes on legend kills."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defcustom wattson--legend-gameplay '(
                                      "And another one down."
                                      "Beau travail. That squad is all down!"
                                      "Beau travail. That was the whole squad!"
                                      "Can't beat the view from up here!"
                                      "Charge!"
                                      "Fifty percent of the squads have been eliminated."
                                      "First kill. On the bright side, it wasn't us."
                                      "Good to be back."
                                      "Guess I took out the whole squad."
                                      "Half the squads have fallen. My odds are getting better."
                                      "I defeated an adversary! We are one step closer to winning."
                                      "I downed an adversary."
                                      "I feel... recharged!"
                                      "I killed an enemy. Not bad for an electrical engineer, no?"
                                      "I made kill leader on my own. I guess I learned a few things after all."
                                      "I took an enemy down. Pretty good for an electrical engineer. No?"
                                      "It's the final squad... and me."
                                      "Just me. Alone against two enemy squads."
                                      "Last one down. Ouais!"
                                      "Let's go!"
                                      "Let's go. Allons-y."
                                      "More down. It pays to have grown up around here."
                                      "More enemies down."
                                      "My circuits are connecting again! Thank you."
                                      "My synapses are firing again! Thank you."
                                      "Only one other team left."
                                      "Player is revived"
                                      "That was the last one! Bye-bye squad."
                                      "That's the first of many lights to go out."
                                      "The whole squad's been shut down."
                                      "We took first blood. Our circuits work well together."
                                      "Woohoo!"
                                      )
  "List of all Wattson quotes on legend gameplay."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defcustom wattson--legend-abilities '(
                                       "A circuit was compromised! We are not alone."
                                       "A circuit's been broken. We've got company."
                                       "A fence circuit's broken out there!"
                                       "Anyone passes through here, we'll know."
                                       "Circuit's broken! Someone else is here."
                                       "Combat's too noisy. Hush now."
                                       "Eye of the storm."
                                       "Fence in position."
                                       "Fence placed."
                                       "Fence them in, fence them out."
                                       "Hush now. No more storms."
                                       "I'm fencing."
                                       "If the fence gets crossed, we'll know."
                                       "Laying fences."
                                       "My fence circuit was broken out there!"
                                       "Negating all incoming artillery. Sweet silence."
                                       "One of my far fences was crossed."
                                       "One of my far off fences was crossed!"
                                       "One of my fence circuits out there was crossed!"
                                       "Placing a fence."
                                       "Playing with fences."
                                       "Putting down a fence."
                                       "Setting a fence."
                                       "Someone crossed a fence! Get ready."
                                       "Someone crossed a fence. Get ready."
                                       "Someone crossed one of my distant fences."
                                       "Someone crossed one of my far off fences."
                                       "That's a funny fence!"
                                       "That's a good fence!"
                                       "That's a grumpy fence."
                                       "That's a happy fence!"
                                       "That's a nice fence."
                                       "That's a pretty fence."
                                       "That's a sad fence."
                                       "That's a scary fence..."
                                       "That's a silly fence!"
                                       "That's an angry fence."
                                       "The circuit is complete."
                                       "The fence is complete."
                                       )
  "List of all Wattson quotes on legend abilities."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defcustom wattson--legend-celebrations '(
                                          "We kicked serious butt! I learned that from the last champion."
                                          "Bon boulot!"
                                          "C'est genial. We work well together. "
                                          )
  "List of all Wattson quotes on legend celebrations."
  :group 'apex-legends--wattson
  :type '(repeat string))

(defvar wattson--legend-voice-lines)

(setq wattson--legend-voice-lines (vconcat
                                   wattson--legend-selection
                                   wattson--legend-intro
                                   wattson--legend-kills
                                   wattson--legend-gameplay
                                   wattson--legend-abilities
                                   wattson--legend-celebrations
                                   ))

(defcustom wattson--byline " - Natalie 'Wattson' Paquette"
  "This string gets concatenated to the actual quotes."
  :group 'apex-legends--wattson
  :type 'string)

(defun random-wattson-quotes ()
  "Return random Wattson quote."
  (concat (random-from-vector wattson--legend-voice-lines) wattson--byline))

;; ------------------------------------------------
;;
;;;###autoload
(defun get-random-apex-legends-quote ()
  "Get random quote from any Apex Legends character."
  (interactive)
  (random-mirage-quotes)
  (cl-case (random 6)
    (0 (random-mirage-quotes))
    (1 (random-wraith-quotes))
    (2 (random-lifeline-quotes))
    (3 (random-bangalore-quotes))
    (4 (random-octane-quotes))
    (5 (random-wattson-quotes))
    (t "Fatal error")))


(provide 'apex-legends-quotes)

;; -----------------------------------------------------------------------------
;;; apex-legends-quotes.el ends here
